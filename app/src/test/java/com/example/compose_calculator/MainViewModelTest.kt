package com.example.compose_calculator

import androidx.activity.viewModels
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class MainViewModelTest {

    @Test
    @DisplayName(" Test if method returns sum of two strings")
    fun requirement1(){
        //Given
        val sum = (("2").toInt() + ("4").toInt()).toString()
        //When
        val testSum = add("2", "4")

        //Then
        Assertions.assertEquals(sum, testSum)

    }

    @Test
    @DisplayName(" Test if method returns result of subtracted strings")
    fun requirement2(){
        //Given
        val quotient = (("6").toInt() - ("2").toInt()).toString()
        //When
        val testQuotient = subtract("6", "2")

        //Then
        Assertions.assertEquals(quotient, testQuotient)

    }

    @Test
    @DisplayName(" Test if method returns error message")
    fun requirement3(){
        //Given
        val quotient = "This calculator only returns positive integers"
        //When
        val testQuotient = subtract("6", "8")

        //Then
        Assertions.assertEquals(quotient, testQuotient)

    }

    @Test
    @DisplayName(" Test if method returns multiplication result")
    fun requirement4(){
        //Given
        val result = (("8").toInt()*("3").toInt()).toString()
        //When
        val testResult = multiply("8", "3")

        //Then
        Assertions.assertEquals(result, testResult)

    }
    @Test
    @DisplayName(" Test if division result returns ")
    fun requirement5(){

        //Given
        val result = (("8").toInt()/("4").toInt()).toString()
        //When
        val testResult = divide("8", "4")

        //Then
        Assertions.assertEquals(result, testResult)

    }

    @Test
    @DisplayName(" Test if division result returns error")
    fun requirement6(){

        //Given
        val result = "Unabe to Compute"
        //When
        val testResult = divide("8", "0")

        //Then
        Assertions.assertEquals(result, testResult)

    }

    fun multiply(x:String, y:String) : String {
        var result =""
        if( x != "" && y != "") {
            val newX = x.toInt()
            val newY = y.toInt()
            result  = (newX * newY).toString()
        }
        return result
    }

    fun add(x:String, y:String) : String {
        var result = ""
        if( x != "" && y != "") {
            val newX = x.toInt()
            val newY = y.toInt()

            result = (newX + newY).toString()
        }
        return result
    }

    fun subtract(x:String, y:String) : String {
        var result = ""
        if( x != "" && y != "") {
            val newX = x.toInt()
            val newY = y.toInt()
            if (newX >= newY) {
                result = (newX - newY).toString()
            } else {
                result = "This calculator only returns positive integers"
            }
        }
        return result
    }

    fun divide(x:String, y:String) : String  {
        var result = ""
        if( x != "" && y != "") {
            val newX = x.toInt()
            val newY = y.toInt()
            if (newY != 0) {
                val long = (newX / newY).toLong()
                result = long.toString()
            } else {result ="Unabe to Compute"}
        }
        return result
    }
}
