package com.example.compose_calculator

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainViewModel : ViewModel() {

    private val _result = MutableStateFlow("0")
    val result: StateFlow<String> get() = _result

    fun add(x:String, y:String) : String {
        if( x != "" && y != "") {
            val newX = x.toInt()
            val newY = y.toInt()

            _result.value = (newX + newY).toString()
        }
        return _result.value
    }

    fun subtract(x:String, y:String) : String {
        if( x != "" && y != "") {
            val newX = x.toInt()
            val newY = y.toInt()
            if (newX >= newY) {
                _result.value = (newX - newY).toString()
            } else {
                _result.value = "This calculator only returns positive integers"
            }
        }
        return _result.value.toString()
    }

    fun multiply(x:String, y:String) : String {
        if( x != "" && y != "") {
            val newX = x.toInt()
            val newY = y.toInt()
            _result.value = (newX * newY).toString()
        }
        return _result.value
    }
    fun divide(x:String, y:String) : String  {
        if( x != "" && y != "") {
            val newX = x.toInt()
            val newY = y.toInt()
            if (newY != 0) {
                val long = (newX / newY).toLong()
                _result.value = long.toString()
            } else {_result.value="Unable to Compute"}
        }
        return _result.value
    }

    fun clear() {
        _result.value = "0"
    }
}
