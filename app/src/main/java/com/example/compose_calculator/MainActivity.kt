package com.example.compose_calculator

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuBox
import androidx.compose.material.ExposedDropdownMenuDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.compose_calculator.ui.theme.Compose_CalculatorTheme

class MainActivity : ComponentActivity() {
    private val mainViewModel by viewModels<MainViewModel>()
    var addSum: (x:String, y:String)-> String = { s: String, s1: String -> mainViewModel.add(x=s, y=s1) }
    var subtract: (x:String, y:String)-> String = { s: String, s1: String -> mainViewModel.subtract(x=s, y=s1) }
    var multiply: (x:String, y:String)-> String = { s: String, s1: String -> mainViewModel.multiply(x=s, y=s1) }
    var divide: (x:String, y:String)-> String = { s: String, s1: String -> mainViewModel.divide(x=s, y=s1) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Compose_CalculatorTheme {
                val result : String by mainViewModel.result.collectAsState()
                println("Flow of Results are $result")
                val intResult = result.toInt()

                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    CalculatorScreen(result = result, addSum = addSum,multiply = multiply, divide = divide, subtract = subtract)
                }
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Compose_CalculatorTheme {
    }
}

@Composable
fun Label() {
    Text(text = "Input Number")
}

@SuppressLint("SuspiciousIndentation")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CalculatorScreen(result: String, addSum: (x:String, y:String) -> String, subtract: (x:String, y:String) -> String, multiply: (x:String, y:String) -> String, divide: (x:String, y:String) -> String) {

    val contextForToast = LocalContext.current.applicationContext
    var inputX:Int = 0
    var inputY:Int = 0

    val listItems = arrayOf("+", "-", "x", "÷")

    var selectedItem by remember {
        mutableStateOf(listItems[0])
    }

    var expanded by remember {
        mutableStateOf(false)
    }
    var value: String by remember {
        mutableStateOf("")
    }

    var valuetwo: String by remember {
        mutableStateOf("")
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column {
            Text(text = "Simple Calculator", textAlign = TextAlign.Center)
            TextField(value = value, onValueChange = {
                println(it)
                value = it
                inputX.toInt()}
                , label = { Label() })
            // the box
            ExposedDropdownMenuBox(
                expanded = expanded,
                onExpandedChange = {
                    expanded = !expanded
                }
            ) {

                // text field
                TextField(
                    value = selectedItem,
                    onValueChange = {},
                    readOnly = true,
                    label = { Text(text = "Label") },
                    trailingIcon = {
                        ExposedDropdownMenuDefaults.TrailingIcon(
                            expanded = expanded
                        )
                    },
                    colors = ExposedDropdownMenuDefaults.textFieldColors()
                )

                // menu
                ExposedDropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }
                ) {
                    listItems.forEach { selectedOption ->
                        // menu item

                        DropdownMenuItem(onClick = {
                            selectedItem = selectedOption
                            when (selectedOption) {
                                "+" -> {
                                    println ("value = $value")
                                    println ("valueTwo = $valuetwo")
//                                    println("running add function  ${addSum(value, valuetwo)}")
//                                    addSum(value, valuetwo)
                                    println(" Addition selected result = $result")


                                }
                                "-" -> {
                                    println ("value = $value")
                                    println ("valueTwo = $valuetwo")
//                                    println("  ${subtract(value, valuetwo)}")
//                                    subtract(value,valuetwo)

                                    println(" Subtraction selected result = $result")

                                }
                                "x" -> {
                                    println ("value = $value")
                                    println ("valueTwo = $valuetwo")
//                                    multiply(value, valuetwo)
                                    println(" Multiplication selected result = $result")

                                }
                                "÷" -> {
                                    println ("value = $value")
                                    println ("valueTwo = $valuetwo")
//                                    divide(value, valuetwo)
                                    println(" division selected result = $result")

                                }

                            }
                            Toast.makeText(contextForToast, selectedOption, Toast.LENGTH_SHORT).show()
                            expanded = false
                        }) {
                            Text(text = selectedOption)
                        }
                    }
                }
            }
            TextField(value = valuetwo, onValueChange = { valuetwo = it
                                                            println(it)
                                                     inputY.toInt()}, label = { Label() })
            Button(onClick = {
                when (selectedItem) {
                "+" -> {
                    println ("value = $value")
                    println ("valueTwo = $valuetwo")
                    println("running add function  ${addSum(value, valuetwo)}")
                    addSum(value, valuetwo)
                    println(" Addition selected result = $result")


                }
                "-" -> {
                    println ("value = $value")
                    println ("valueTwo = $valuetwo")
                    println("  ${subtract(value, valuetwo)}")

                    println(" Subtraction selected result = $result")

                }
                "x" -> {
                    println ("value = $value")
                    println ("valueTwo = $valuetwo")
                    multiply(value, valuetwo)
                    println(" Multiplication selected result = $result")

                }
                "÷" -> {
                    println ("value = $value")
                    println ("valueTwo = $valuetwo")
                    divide(value, valuetwo)
                    println(" division selected result = $result")

                }

            }}, modifier = Modifier.padding(horizontal = 100.dp, vertical = 25.dp)) {
                Text(text = "Calculate")
            }
            Text(text = "= $result", fontSize = 25.sp, modifier = Modifier.padding(horizontal = 130.dp, vertical = 50.dp))

        }
    }
}


